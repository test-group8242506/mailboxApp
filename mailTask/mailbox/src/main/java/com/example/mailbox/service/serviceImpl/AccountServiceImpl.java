package com.example.mailbox.service.serviceImpl;

import com.example.mailbox.dto.UserDto;
import com.example.mailbox.entity.Account;
import com.example.mailbox.feign.AuthCallableApi;
import com.example.mailbox.repository.AccountRepository;
import com.example.mailbox.response.CreateAccountResponse;
import com.example.mailbox.response.Meta;
import com.example.mailbox.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    AuthCallableApi authCallableApi;

    @Autowired
    AccountRepository accountRepository;


    public CreateAccountResponse createMailBox(UserDto userDto){
        return getCreateAccountResponse(userDto);
    }
    public CreateAccountResponse getMailBox(UserDto userDto){
        return null;
    }

    private CreateAccountResponse getCreateAccountResponse(UserDto userDto) {
        return null;
    }

    @Override
    public Account findAccount(int userId) {
        return accountRepository.findByUserId(userId);
    }


}


